#!/bin/bash -e

echo -e "\n\n===================================================="

echo "Dot-sourcing functions"
DIRECTORY=$(dirname $0)
. $PWD/$DIRECTORY/functions.sh

os="Linux_x86_64"
gh_repo="accurics/terrascan"

if [ -z "${TERRASCAN_VERSION}" ] || [ "${TERRASCAN_VERSION}" == "latest" ]; then
  echo "Looking up the latest version ..."
  version=$(get_latest_release $gh_repo)
else
  version="v${TERRASCAN_VERSION}"
fi

# Since Terrascan doesn't use the 'v' of the version number in the filename, we 
# have to strip that first character. We're using 'cut' for that.
release_file="terrascan_$(echo "$version" | cut -c 2-)_${os}"
extension=".tar.gz"
download_uri="https://github.com/${gh_repo}/releases/download/${version}/${release_file}${extension}"

echo "Downloading terrascan $version (${release_file}) from ${download_uri}"
curl --fail --silent -L -o /tmp/terrascan${extension} "${download_uri}"
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "Failed to download ${release_file}"
  exit $retVal
else
  echo "Downloaded successfully"
fi

echo -e "\n\n----------------------------------------------------"
dest="${TERRASCAN_INSTALL_PATH:-/usr/local/bin}/"
echo "Installing /tmp/terrascan to ${dest}..."

if [[ -w "$dest" ]]; then SUDO=""; else
  # current user does not have write access to install directory
  SUDO="sudo";
fi

$SUDO mkdir -p "$dest"
$SUDO tar -xzf /tmp/terrascan${extension} --directory=/tmp/
$SUDO chmod +x /tmp/terrascan
$SUDO mv /tmp/terrascan "$dest"
"${dest}/terrascan" init
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "Failed to install terrascan"
  exit $retVal
else
  echo "terrascan installed at ${dest} successfully"
fi

echo "Cleaning /tmp/terrascan.tar.gz and /tmp/terrascan ..."
rm -f /tmp/terrascan.tar.gz /tmp/terrascan

echo -e "\n\n----------------------------------------------------"
echo "Current terrascan version"
"${dest}/terrascan" version

echo -e "\nLatest version available: $(get_latest_release $gh_repo)\n"

echo "terrascan: ${version}" >> ${VERSION_FILE}
