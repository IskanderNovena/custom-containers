#!/bin/bash -e

echo -e "\n\n===================================================="

echo "Dot-sourcing functions"
DIRECTORY=$(dirname $0)
. $PWD/$DIRECTORY/functions.sh

os="linux-amd64"
gh_repo="infracost/infracost"

if [ -z "${INFRACOST_VERSION}" ] || [ "${INFRACOST_VERSION}" == "latest" ]; then
  echo "Looking up the latest version ..."
  version=$(get_latest_release)
else
  version="v${INFRACOST_VERSION}"
fi

release_file="infracost-${os}"
extension=".tar.gz"
download_uri="https://github.com/${gh_repo}/releases/download/${version}/${release_file}${extension}"

echo "Downloading infracost $version (${release_file}) from ${download_uri}"
curl --fail --silent -L -o /tmp/infracost${extension} "${download_uri}"
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "Failed to download ${release_file}"
  exit $retVal
else
  echo "Downloaded successfully"
fi

echo -e "\n\n----------------------------------------------------"
dest="${INFRACOST_INSTALL_PATH:-/usr/local/bin}/"
echo "Installing /tmp/infracost${extension} to ${dest}..."

if [[ -w "$dest" ]]; then SUDO=""; else
  # current user does not have write access to install directory
  SUDO="sudo";
fi

$SUDO mkdir -p "$dest"
$SUDO tar -xzf /tmp/infracost${extension} --directory=/tmp/
$SUDO mv /tmp/infracost-linux-amd64 /tmp/infracost
$SUDO chmod +x /tmp/infracost
$SUDO mv /tmp/infracost "$dest"
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "Failed to install infracost"
  exit $retVal
else
  echo "infracost installed at ${dest} successfully"
fi

echo "Cleaning /tmp/infracost.tar.gz and /tmp/infracost ..."
rm -f /tmp/infracost.tar.gz /tmp/infracost

echo -e "\n\n----------------------------------------------------"
echo "Current infracost version"
"${dest}/infracost" --version

echo -e "\nLatest version available: $(get_latest_release $gh_repo)\n"

echo "infracost: ${version}" >> ${VERSION_FILE}
