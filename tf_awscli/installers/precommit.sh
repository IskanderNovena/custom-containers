#!/bin/bash
echo -e "\n\n===================================================="

echo "Dot-sourcing functions"
DIRECTORY=$(dirname $0)
. $PWD/$DIRECTORY/functions.sh

echo -e "\n\n----------------------------------------------------"

echo "Installing pre-commit"

# Install pre-commit through pip
pip install --no-cache-dir -q pre-commit==${PRECOMMIT_VERSION}
echo -e "\nLatest version available: $(get_latest_release pre-commit/pre-commit)\n"
echo "pre-commit: ${PRECOMMIT_VERSION}" >> ${VERSION_FILE}

echo -e "\n\n----------------------------------------------------"

echo "Installing checkov"

# Install checkov through pip
pip3 install --no-cache-dir -q checkov==${CHECKOV_VERSION}
echo -e "\nLatest version available: $(get_latest_release bridgecrewio/checkov)\n"
echo "checkov: ${CHECKOV_VERSION}" >> ${VERSION_FILE}

# Install tflint. This uses the TFLINT_VERSION variable to install the specified
# version. Otherwise it will install the latest version.
# curl -s https://raw.githubusercontent.com/terraform-linters/tflint/master/install_linux.sh | bash
/installers/tflint_install_script.sh

# Install tfsec. This uses the TFSEC_VERSION variable to install the specified
# version. Otherwise it will install the latest version. The install-script is
# derived from the tflint install-script.
# curl --location https://github.com/aquasecurity/tfsec/releases/download/v${TFSEC_VERSION}/tfsec-linux-amd64 --output /usr/local/bin/tfsec
# chmod u+x 
/installers/tfsec_install_script.sh

# Install terraform-docs. This uses the TERRAFORM_DOCS_VERSION variable to 
# install the specified version. Otherwise it will install the latest version. 
# The install-script is derived from the tflint install-script
/installers/terraform_docs_install_script.sh

# Install terrascan. This uses the TERRASCAN_VERSION variable to 
# install the specified version. Otherwise it will install the latest version. 
# The install-script is derived from the tflint install-script
/installers/terrascan_install_script.sh

# Install infracost. This uses the INFRACOST_VERSION variable to install the 
# specified version. Otherwise it will install the latest version.
/installers/infracost_install_script.sh
# The original project script doesn't allow for specific versions.
# curl -fsSL https://raw.githubusercontent.com/infracost/infracost/master/scripts/install.sh | bash
