#!/bin/bash
echo -e "\n\n===================================================="

echo "Dot-sourcing functions"
DIRECTORY=$(dirname $0)
. $PWD/$DIRECTORY/functions.sh

echo "Downloading Terraform ${TERRAFORM_VERSION} (terraform.zip) from https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip"

# Install Terraform
curl --silent --location https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip --output /tmp/terraform.zip

echo -e "\n\n----------------------------------------------------"

echo "Installing Terraform"

unzip -qq /tmp/terraform.zip -d /usr/local/bin
rm -f /tmp/terraform.zip

# Install jq
echo "Installing jq"
apt install -y -qq jq > /dev/null
# -o Dpkg::Use-Pty=0

# Add custom command for a specific JQ command used with Terraform
echo "Installing custom command 'convert_report'"
echo "jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'" > /usr/local/bin/convert_report
chmod u+x /usr/local/bin/convert_report

echo -e "\n\n----------------------------------------------------"
echo "Current Terraform version"
terraform --version

echo "Terraform: ${TERRAFORM_VERSION}" >> ${VERSION_FILE}
