#!/bin/bash
echo -e "\n\n===================================================="

echo "Dot-sourcing functions"
DIRECTORY=$(dirname $0)
. $PWD/$DIRECTORY/functions.sh

echo "Downloading AWS CLI ${AWSCLI_VERSION} (awscliv2.zip) from https://awscli.amazonaws.com/awscli-exe-linux-x86_64-${AWSCLI_VERSION}.zip"

# Install AWS CLI
curl --silent --location "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-${AWSCLI_VERSION}.zip" --output "/tmp/awscliv2.zip"

echo -e "\n\n----------------------------------------------------"

echo "Installing AWS CLI"
unzip -qq /tmp/awscliv2.zip -d /tmp
/tmp/aws/install
rm -rf /tmp/awscliv2.zip /tmp/aws/


echo -e "\n\n----------------------------------------------------"
echo "Current AWS CLI version"
aws --version

echo "AWS CLI: ${AWSCLI_VERSION}" >> ${VERSION_FILE}

