get_latest_release() {
  # Provide the owner and repository-name in the format "owner/repository" for the GitHub repository
  local gh_repo=$1

  curl --silent "https://api.github.com/repos/${gh_repo}/releases/latest" | # Get latest release from GitHub api
    grep '"tag_name":' |                                                    # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                            # Pluck JSON value
}
