#!/bin/bash -e

echo -e "\n\n===================================================="

echo -e "\nDot-sourcing functions"
DIRECTORY=$(dirname $0)
. $PWD/$DIRECTORY/functions.sh

os="linux_amd64"
gh_repo="terraform-linters/tflint"

if [ -z "${TFLINT_VERSION}" ] || [ "${TFLINT_VERSION}" == "latest" ]; then
  echo "Looking up the latest version ..."
  version=$(get_latest_release $gh_repo)
else
  version="v${TFLINT_VERSION}"
fi

release_file="tflint_${os}"
extension=".zip"
download_uri="https://github.com/${gh_repo}/releases/download/${version}/${release_file}${extension}"

echo "Downloading tflint $version (${release_file}) from ${download_uri}"
curl --fail --silent -L -o /tmp/tflint${extension} "${download_uri}"
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "Failed to download ${release_file}${extension}"
  exit $retVal
else
  echo "Downloaded successfully"
fi

echo -e "\n\n----------------------------------------------------"

dest="${TFSEC_INSTALL_PATH:-/usr/local/bin}/"
echo "Installing /tmp/tflint${extension} to ${dest}..."

if [[ -w "$dest" ]]; then SUDO=""; else
  # current user does not have write access to install directory
  SUDO="sudo";
fi

$SUDO mkdir -p "$dest"
$SUDO unzip -qq /tmp/tflint${extension} -d ${dest}
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "Failed to install tflint"
  exit $retVal
else
  echo "tflint installed at ${dest} successfully"
fi

echo "Cleaning /tmp/tflint ..."
rm -f /tmp/tflint

echo -e "\n\n----------------------------------------------------"
echo "Installed tflint version"
"${dest}/tflint" -v

echo -e "\nLatest version available: $(get_latest_release $gh_repo)\n"

echo "tflint: ${version}" >> ${VERSION_FILE}
