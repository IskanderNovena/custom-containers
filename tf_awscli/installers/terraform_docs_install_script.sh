#!/bin/bash -e

echo -e "\n\n===================================================="

echo -e "\nDot-sourcing functions"
DIRECTORY=$(dirname $0)
. $PWD/$DIRECTORY/functions.sh

os="linux-amd64"
gh_repo="terraform-docs/terraform-docs"

if [ -z "${TERRAFORM_DOCS_VERSION}" ] || [ "${TERRAFORM_DOCS_VERSION}" == "latest" ]; then
  echo "Looking up the latest version ..."
  version=$(get_latest_release $gh_repo)
else
  version="v${TERRAFORM_DOCS_VERSION}"
fi

release_file="terraform-docs-${version}-${os}"
extension=".tar.gz"
download_uri="https://github.com/${gh_repo}/releases/download/${version}/${release_file}${extension}"

echo "Downloading terraform-docs $version (${release_file}) from ${download_uri}"
curl --fail --silent -L -o /tmp/terraform-docs${extension} "${download_uri}"
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "Failed to download ${release_file}"
  exit $retVal
else
  echo "Downloaded successfully"
fi

echo -e "\n\n----------------------------------------------------"
dest="${TERRAFORM_DOCS_INSTALL_PATH:-/usr/local/bin}/"
echo "Installing /tmp/terraform-docs${extension} to ${dest}..."

if [[ -w "$dest" ]]; then SUDO=""; else
  # current user does not have write access to install directory
  SUDO="sudo";
fi

$SUDO mkdir -p "$dest"
$SUDO tar -xzf /tmp/terraform-docs${extension} --directory=/tmp/
$SUDO chmod +x /tmp/terraform-docs
$SUDO mv /tmp/terraform-docs "$dest"
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "Failed to install terraform-docs"
  exit $retVal
else
  echo "terraform-docs installed at ${dest} successfully"
fi

echo "Cleaning /tmp/terraform-docs.tar.gz and /tmp/terraform-docs ..."
rm -f /tmp/terraform-docs.tar.gz /tmp/terraform-docs

echo -e "\n\n----------------------------------------------------"
echo "Installed terraform-docs version"
"${dest}/terraform-docs" -v

echo -e "\nLatest version available: $(get_latest_release $gh_repo)\n"

echo "terraform-docs: ${version}" >> ${VERSION_FILE}
