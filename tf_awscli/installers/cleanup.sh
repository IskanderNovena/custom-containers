#!/bin/bash
# Cleanup after all packages are installed
apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* *.zip
