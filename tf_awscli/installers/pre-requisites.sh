#!/bin/bash
# Script to install the pre-requisites for installing other packages
echo "Installing package updates"
apt update -y -qq > /dev/null
# -o Dpkg::Use-Pty=0

echo "Installing pre-requisite packages"
apt install -y -qq curl unzip wget > /dev/null
# -o Dpkg::Use-Pty=0

echo "Setting timezone"
# Set localtime to CEST
# This is needed for the Python-installation, which includes tzdata
ln -fs /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime

echo "Installing Python and pip"
apt install -y -qq python3 python3-pip software-properties-common > /dev/null
# -o Dpkg::Use-Pty=0
# python3-distutils python3-apt