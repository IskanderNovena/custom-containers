#!/bin/bash -e

echo -e "\n\n===================================================="

echo -e "\nDot-sourcing functions"
DIRECTORY=$(dirname $0)
. $PWD/$DIRECTORY/functions.sh

os="linux-amd64"
gh_repo="aquasecurity/tfsec"

if [ -z "${TFSEC_VERSION}" ] || [ "${TFSEC_VERSION}" == "latest" ]; then
  echo "Looking up the latest version ..."
  version=$(get_latest_release $gh_repo)
else
  version="v${TFSEC_VERSION}"
fi

release_file="tfsec-${os}"
extension=""
download_uri="https://github.com/${gh_repo}/releases/download/${version}/${release_file}${extension}"

echo "Downloading tfsec $version (${release_file}) from ${download_uri}"
curl --fail --silent -L -o /tmp/tfsec${extension} "${download_uri}"
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "Failed to download ${release_file}${extension}"
  exit $retVal
else
  echo "Downloaded successfully"
fi

echo -e "\n\n----------------------------------------------------"

dest="${TFSEC_INSTALL_PATH:-/usr/local/bin}/"
echo "Installing /tmp/tfsec${extension} to ${dest}..."

if [[ -w "$dest" ]]; then SUDO=""; else
  # current user does not have write access to install directory
  SUDO="sudo";
fi

$SUDO mkdir -p "$dest"
$SUDO install -c -v /tmp/tfsec${extension} "$dest"
retVal=$?
if [ $retVal -ne 0 ]; then
  echo "Failed to install tfsec"
  exit $retVal
else
  echo "tfsec installed at ${dest} successfully"
fi

echo "Cleaning /tmp/tfsec ..."
rm -f /tmp/tfsec

echo -e "\n\n----------------------------------------------------"
echo "Installed tfsec version"
"${dest}/tfsec" -v

echo -e "\nLatest version available: $(get_latest_release $gh_repo)\n"

echo "tfsec: ${version}" >> ${VERSION_FILE}
