# Changelog for tf_awscli container

## 1.0 - 2021-09-07

Initial release.  

Container based on Ubuntu:21.04 with the following packages added:  

- Terraform 1.0.6
- AWS CLI 2.2.35
- JQ 1.6

An alias has been created for JQ which is used with Terraform to create a report of a plan-file.
